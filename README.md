# AboutMe

## Short Bio

In 1979 not only [Pink Floyd released "The Wall"](https://en.wikipedia.org/wiki/The_Wall) and [Sony the Walkman](https://en.wikipedia.org/wiki/Walkman), but my parents, a math/informatics teacher married to a kindergarten teacher, received me on the second to last day of May.  
Born and raised in the [Lower Rhine region of Germany](https://www.geonames.org/12076814/niederrhein.html), a bycicle ride distant from the [green border with the Netherlands](https://en.wikipedia.org/wiki/Germany%E2%80%93Netherlands_border), I found early in life my passion for [water sports](https://www.delphin-geldern.com/) and [board/roleplay games](https://boardgamegeek.com/user/zwobot).  
Through all my 13 years of school, it turned out, that I have an aptitude for math, physics and informatics. Logic and natural science spurred my interests.
In 1988 my father handed down to me his [Commodore 64](https://en.wikipedia.org/wiki/Commodore_64) home computer and connected to our old color TV it became the magnet for my attention from then on.  
Many hours were spend loading [Mafia the game](https://www.c64-wiki.com/wiki/Mafia) from [datasette](https://en.wikipedia.org/wiki/Commodore_Datasette) and playing with 4 friends hot seat, noses inches from the buzzing TV screen.
With my [Abitur](https://en.wikipedia.org/wiki/Abitur) in 1998 and [Zivildienst](https://en.wikipedia.org/wiki/Zivildienst), as orderly for the elderly, done, I studied [electrical engineering](https://www.elektrotechnik.rwth-aachen.de/) at the RWTH-Aachen.
Earning my livelihood with different side jobs (Life guard at the local pool, flipping burgers in the nightshiftat MC, building and repaiing PCs), I started a scientific assistant position as system administrator for a [mechanical engineering institute](https://www.ikt-aachen-gmbh.de/about-us).

Since 2010 I have been leading hybrid multi-national teams in the IT sector.  
My passions is leading teams with customer centric drive and foster plus advance the teammebers to their best selfs.  

At the moment I am a [T-shaped Professional](https://en.wikipedia.org/wiki/T-shaped_skills), a universal dilettante or universal amateur,
My horizontal line, my base, my domain knowledge is Information Technology.
My passion is documentation.  
Writing, editing and using it. Documentation is the first pillar of success, including customer success.
Write it so your future self can understand it without any context.

## Values and Guiding Principles

[KISS](https://en.wikipedia.org/wiki/KISS_principle) Keep it simple and stupid

## Some of [My Favorite Things](https://www.iro.umontreal.ca/~eckdoug/favorite_things/sound_of_music.mp3)

In my humble opionion you get a good picture of another human being and perhaps some shared interests by knowing there [favorite things](https://en.wikipedia.org/wiki/My_Favorite_Things_(song)). To keep it short and nice, here only my top 3 from the 3 categories I spent most of my quality time with.

### Books

1. [The Hitchhiker's Guide to the Galaxy](https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy) - [Douglas Adams](https://en.wikipedia.org/wiki/Douglas_Adams)
2. [Cryptonomicon](https://en.wikipedia.org/wiki/Cryptonomicon) - [Neal Stephenson](https://en.wikipedia.org/wiki/Neal_Stephenson)
3. [Going Postal](https://en.wikipedia.org/wiki/Going_Postal) - [Terry Pratchett](https://en.wikipedia.org/wiki/Terry_Pratchett)

### Boardgames

1. [Saint Petersburg (2004)](https://boardgamegeek.com/boardgame/9217/saint-petersburg) - [Bernd Brunnhofer](https://boardgamegeek.com/boardgamedesigner/928/bernd-brunnhofer)
2. [Android: Netrunner (2012)](https://boardgamegeek.com/boardgame/124742/android-netrunner) - [Richard Garfield](https://boardgamegeek.com/boardgamedesigner/14/richard-garfield)
3. [Imperial (2006)](https://boardgamegeek.com/boardgame/24181/imperial) - [Mac Gerdts](https://boardgamegeek.com/boardgamedesigner/6045/mac-gerdts)

### Songs

1. [Don't Stop Me Now (1979)](https://www.youtube.com/watch?v=HgzGwKwLmgM) - [Queen](https://en.wikipedia.org/wiki/Queen_(band))
2. [(10) and Counting (2006)](https://www.youtube.com/watch?v=I0zqbWMDWnc) - [BoySetsFire](https://en.wikipedia.org/wiki/BoySetsFire)
3. [Family Tree (1996)](https://www.youtube.com/watch?v=CQbGYgKOgW8) - [H2O](https://en.wikipedia.org/wiki/H2O_(American_band))
